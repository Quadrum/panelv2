<?php
return array(
    'db' => array(
        'host' => 'localhost',
        'name' => 'panel',
        'user' => '',
        'password' => ''
        ),
    'routes' => array(
        '/' => 'Main::index',
        'admin' => 'Admin::index',
        'product' => 'Product::index',
        '404' => '404::index',
        )
    );

