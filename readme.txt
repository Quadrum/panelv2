Выполнил: Бедов Денис © 2016
mail: develop.fsater@gmail.com
======================================================================
Для работы с базой данных был использован класс (application/core/database.php).

Структура проекта.
.
├── application
│   ├── bootstrap.php
│   ├── controllers
│   │   ├── controller_404.php
│   │   ├── controller_admin.php
│   │   ├── controller_page.php
│   │   └── controller_post.php
│   ├── core
│   │   ├── controller.php
│   │   ├── database.php
│   │   ├── model.php
│   │   ├── funtions.php
│   │   ├── route.php
│   │   └── view.php
│   ├── models
│   │   ├── model_admin.php
│   │   ├── model_comment.php
│   │   ├── model_page.php
│   │   └── model_posts.php
│   └── views
│       ├── 404_view.php
│       ├── admin_login.php
│       ├── admin_view.php
│       ├── main_view.php
│       ├── page_view.php
│       ├── post_view.php
│       └── template_view.php
├── config.php
├── css
│   ├── bootstrap-theme.min.css
│   ├── jquery.tagsinput.min.css
│   ├── login-form.css
│   └── simple-sidebar.css
├── favicon.ico
├── fonts
│   ├── glyphicons-halflings-regular.eot
│   ├── glyphicons-halflings-regular.svg
│   ├── glyphicons-halflings-regular.ttf
│   ├── glyphicons-halflings-regular.woff
│   └── glyphicons-halflings-regular.woff2
├── images
│   ├── 286-75ba0b412818f3beaa2e8f87bf577e0b.jpg
│   └── bg.png
├── index.php
├── js
│   ├── ckeditor		
│   ├── bootstrap.min.js
│   ├── jquery-2.1.4.min.js
│   ├── jquery.tagsinput.min.js
│   └── login-form.js
├── readme.txt
└── vendor
