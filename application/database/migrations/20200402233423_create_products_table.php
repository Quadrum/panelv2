<?php

use Phinx\Migration\AbstractMigration;

class CreateProductsTable extends AbstractMigration
{
    public function up() {
        $table = $this->table('products', ['id' => false, 'primary_key' => ['id']]);
        $table->addColumn('id', 'biginteger',  ['identity' => true])
              ->addColumn('user_id', 'biginteger')
              ->addColumn('name', 'string', ['limit' => 128]) 
              ->addColumn('description', 'text') 
              ->addColumn('price', 'double') 
              ->addColumn('product_adress', 'string', ['limit' => 255]) 
              ->addColumn('owner_adress', 'string', ['limit' => 255]) 
              ->addColumn('trucknumber', 'string', ['limit' => 20]) 
              ->addColumn('product_url', 'string', ['limit' => 255]) 
              ->addColumn('delivery_status', 'string', ['limit' => 255]) 
              ->addColumn('product_category', 'string', ['limit' => 255]) 
              ->addColumn('deal_payment', 'double') 
              ->addColumn('employer_payment', 'double') 
              ->addColumn('admin_payment', 'double') 
              ->addColumn('status', 'boolean') 
              ->addColumn('adddate', 'timestamp',['default' => 'CURRENT_TIMESTAMP']) 
              ->addColumn('archive', 'boolean') 
              ->create();
    }

    public function down() {
        $this->table('products')->drop()->save();
    }
}
