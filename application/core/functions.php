<?php
function debug($var){
    echo '<pre>';
    var_dump($var);
    echo '<pre>';
}

function parseTrack($number) {
    require_once 'phpQuery.php';
    $status = "";
    if (preg_match("#^[0-9]+$#",$number)) {
        $status = json_decode(file_get_contents("https://www.fedex.com/trackingCal/track?data={\"TrackPackagesRequest\":{\"appType\":\"WTRK\",\"appDeviceType\":\"DESKTOP\",\"uniqueKey\":\"\",\"processingParameters\":{},\"trackingInfoList\":[{\"trackNumberInfo\":{\"trackingNumber\":\"$number\",\"trackingQualifier\":\"\",\"trackingCarrier\":\"\"}}]}}&action=trackpackages&locale=en_US&version=1&format=json"));
        $status = $status->TrackPackagesResponse->packageList[0]->keyStatus;
    } else {
        $status = file_get_contents("https://wwwapps.ups.com/WebTracking/track?loc=en_US&track.x=Track&trackNums=".$number);
        $status = phpQuery::newDocument($status);
        $status = trim($status->find('.current h3')->text());
    }
    return $status;
}

function isAdmin() {
    if(isset($_SESSION['is_admin']) && isset($_SESSION['is_admin'])){
        return true;
    } else {
        return false;
    }
}