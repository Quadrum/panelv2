<?php
class Database
{
    protected static $_instance = null;
    public $dbh;
    private function __construct(){
        global $config;
        $this->dbh = new mysqli($config['db']['host'],
                                $config['db']['user'],
                                $config['db']['password'],
                                $config['db']['name']
                                );
        if ($this->dbh->connect_errno) {
            echo "Не удалось подключиться к MySQL: (" . $this->dbh->connect_errno . ") " . $this->dbh->connect_error;
        }
        $this->dbh->query("SET NAMES utf8");
    }
    private function __clone(){
    }

    public static function fetchOne($query,$type = MYSQLI_ASSOC) {
        $result = self::getInstance()->dbh->query($query);
        if (!$result) {
            die(json_encode([
                "query" => self::getInstance()->dbh->info,
                "error" => self::getInstance()->dbh->error
                ]));
        } else {
            return $result->fetch_array($type);
        }
    }

    public static function exec($query){
        self::getInstance()->dbh->query($query);
    }

    public static function query($query,$type=MYSQLI_ASSOC) {
        $query = self::getInstance()->dbh->query($query);
        $result = [];
        if (!$query) {
            die(json_encode([
                "query" => self::getInstance()->dbh->info,
                "error" => self::getInstance()->dbh->error
                ]));
        } elseif(isset($query->num_rows) && $query->num_rows > 0){
            while ($row = $query->fetch_array($type)) {
                $result[] = $row;
            };
            return $result;
        } else {

        }      
    }
    public static function getInstance() {

        if (null === self::$_instance) {
            // создаем новый экземпляр
            self::$_instance = new self();
        }

        return self::$_instance;
    }
}