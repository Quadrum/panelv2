<?php
error_reporting(1);
require_once './database.php';
require_once './functions.php';
define("__TMP__","../../tmp/");
define("__queryCounts__",10);
$config = require_once '../../config.php';

if(!file_exists(__TMP__."tracks")){
    $tracks = Database::query("SELECT id FROM products WHERE status != 3 AND archive != 1");
    foreach($tracks as $key => $track){
        $tracks[$key] = $track['id']."\r\n";
    }
    file_put_contents(__TMP__."tracks", $tracks, FILE_APPEND | LOCK_EX);
    chmod(__TMP__."tracks",0777);
} else {

    $tracks = file(__TMP__."tracks");
    if(!count($tracks)){
        unlink(__TMP__."tracks");
        die();
    }
    for($i = 0; $i < __queryCounts__; $i++){
        if(isset($tracks[$i])) {
            $trucknumber = trim(Database::query("SELECT trucknumber FROM products WHERE id = " . trim($tracks[$i]))[0]['trucknumber']);
            $status = parseTrack($trucknumber);
            Database::exec("UPDATE products SET  delivery_status= \"" . $status . "\" WHERE id = " . trim($tracks[$i]));
            unset($tracks[$i]);
        } else {
            break;
        }
    }
    file_put_contents(__TMP__."tracks", $tracks, LOCK_EX);
}