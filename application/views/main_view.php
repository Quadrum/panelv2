<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $data['title']; ?></title>


    <script src="../../js/jquery.min.js"></script>
    <script src="../../js/jquery.validate.min.js"></script>
    <script src="../../js/jquery-ui.min.js"></script>
    <script src="../../js/bootstrap.min.js"></script>
    <script src="../../js/jquery.dataTables.min.js"></script>
    <script src="../../js/dataTables.bootstrap.js"></script>
    <link rel="stylesheet" href="../../css/jquery-ui.css">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../../css/dataTables.bootstrap.css">
    <link rel="stylesheet" href="../../css/style.css">
    <style type="text/css">
        #dialog-form {
            display: none;
        }
        td {
            white-space: nowrap;
        }
        .bigcontent {
            width: 124px;
            overflow-x: scroll;
            height:48px;
        }
        #d_tbl_wrapper .row .col-sm-12 {
            overflow: auto;
        }
    </style>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {
            $('#d_tbl').dataTable({
                "aaSorting": [[0, "desc"]]
            });
            $('#d_tbl_filter').prepend('<button type="button" class="btn btn-success" onclick="new_product()">Добавить</button>&nbsp;');
        });

        function validate(){
            $("#dl-form").validate({
                wrapper: 'span',
                rules: {
                    name: {
                        required: true
                    },
                    price: {
                        required: true,
                        min: 1000
                    },
                    trucknumber: {
                        required: true
                    }
                },
                messages: {
                    name: "поле не может быть пустым",
                    price: "цена дожна быть больше 1000",
                    trucknumber: "поле не может быть пустым"
                }
            });
        }

        function new_product(){
            dialog = $( "#dialog-form" ).dialog({
                autoOpen: false,
                height: 700,
                width: 700,
                modal: true,
                closeOnEscape: false,
                dialogClass: "noclose",
                buttons : [
                    {
                        text: "Сохранить",
                        click: function() {
                            validate();
                            if($("#dl-form").valid()){
                                product_save();
                            };

                        }
                    },
                    {
                        text: "Закрыть",
                        click: function() {
                            $("#dl-form").trigger("reset");
                            dialog.dialog("close");
                            location.reload();
                        }
                    }
                ],
                open: function(){
                    $(".ui-dialog-titlebar-close").css("display","none");
                }
            });
            dialog.dialog("open");
        }
        /*function product_delete(postId) {
            $.ajax({
                type: 'POST',
                url: '/product/product_delete',
                data:  {'id': postId},
                dataType: "json",
                success: function(data, textStatus){
                    //console.log(data);
                    location.reload();
                }
            });
            return false;
        }*/

        function product_save()
        {
            proddata = {
                id: $("#id").val(),
                name: $("#name").val(),
                price: $("#price").val(),
                product_adress: $("#product_adress").val(),
                owner_adress: $("#owner_adress").val(),
                trucknumber: $("#trucknumber").val(),
                product_url: $("#product_url").val(),
                product_category: $("#product_category").val()
            }
            $.ajax({
                type: 'POST',
                url: '/product/product_save',
                data: proddata,
                dataType: "json",
                success: function (data, textStatus) {
                    if(data.status) {
                        dialog.dialog("close");
                        location.reload();
                    } else {
                        //console.log(data);
                    }
                },
            });

        }
        function product_edit(prodId) {
            dialog = $( "#dialog-form" ).dialog({
                autoOpen: false,
                closeOnEscape: false,
                height: 700,
                width: 700,
                modal: true,
                buttons : [
                {
                    text: "Сохранить",
                    click: function() {
                        validate();
                        if($("#dl-form").valid()){
                            product_save();
                        };
                    }
                },
                {
                    text: "Закрыть",
                    click: function() {
                        $("#dl-form").trigger("reset");
                        dialog.dialog("close");
                        location.reload();
                    }
                }
                ],
                close: function() {
                    dialog.dialog("close");
                },
                open: function(){
                    $(".ui-dialog-titlebar-close").css("display","none");
                }
            });
            $.ajax({
                type: 'POST',
                url: '/product/product_edit',
                data: {'prodid': prodId},
                dataType: "json",
                success: function (data, textStatus) {
           //         console.log(data);
                    $("#id").val(data[0].id);
                    $("#name").val(data[0].name);
                    $("#price").val(data[0].price);
                    $("#product_adress").val(data[0].product_adress);
                    $("#owner_adress").val(data[0].owner_adress);
                    $("#trucknumber").val(data[0].trucknumber);
                    $("#product_url").val(data[0].product_url);
                    $("#product_category option[value='"+ data[0].product_category +"']").attr('selected', 'true').text(data[0].product_category);
                    dialog.dialog("open");
                }
            });
            return false;
        }
    </script>
    <!-- Custom CSS -->
    <link href="../../css/simple-sidebar.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="../../js/html5shiv.js"></script>
    <script src="../../js/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <!-- END DataTables -->
    <div id="dialog-form" title="Товар">
        <form id="dl-form">
            <fieldset>
                <div class="form-group">
                    <input type="hidden" name="id" id="id" value="" class="text ui-widget-content ui-corner-all">
                    <label for="name">ФИО:</label><br>
                    <input type="text" name="name" id="name" value="" class="form-control"><br>
                    <label for="price">Цена:</label><br>
                    <input type="text" name="price" id="price" value="" class="form-control"><br>
                    <label for="product_adress">Адресс товара:</label><br>
                    <input type="text" name="product_adress" id="product_adress" value="" class="form-control"><br>
                    <label for="owner_adress">Адресс владельца:</label><br>
                    <input type="text" name="owner_adress" id="owner_adress" value="" class="form-control"><br>
                    <label for="trucknumber">Трек номер:</label><br>
                    <input type="text" name="trucknumber" id="trucknumber" value="" class="form-control"><br>
                    <label for="product_url">URL товара:</label><br>
                    <input type="text" name="product_url" id="product_url" value="" class="form-control"><br>
                    <!--<label for="status">Статус товара:</label><br>
                    <input type="text" name="status" id="status" value="" class="form-control"><br>-->
                    <label for="product_category">Категория товара:</label>
                    <select class="form-control" id="product_category" name="product_category">
                        <option value="Apple">Apple</option>
                        <option value="Не Apple">Не Apple</option>
                    </select>
                </div>

                <!-- Allow form submission with keyboard without duplicating the dialog button -->
            </fieldset>
        </form>
    </div>
    <nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Panel</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        
        <li><span class="navbar-text">Баланс : <span style="color:green"><b><?=$_SESSION['user_balance']?></span></b></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Действия <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><span class="navbar-text">Пользователь: <?=$_SESSION['user_name']?></span></li>
            <li><a href="/main/logout">Выход</a></li>
            <!--<li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>-->
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12" style="overflow-x: auto;">
                <h1>Управление товарами</h1>
                <table class="table table-striped table-bordered" id="d_tbl">
                    <thead>
                        <th style="width: 40px;">id</th>
                        <th>ФИО</th>
                        <th>Цена</th>
                        <th>Адресс товара</th>
                        <th>Адресс владельца</th>
                        <th>Трек номер</th>
                        <th>URL на товар</th>
                        <th>Трекинг</th>
                        <th>Категория</th>
                        <th>Сумма</th>
                        <th>Статус</th>
                        <th>Действие</th>
                    </thead>
                    <tbody>
                    <?php ;
                    if(!is_null($data['products'])) {
                        foreach ($data['products'] as $product) {
                            $status = [];
                              switch ($product['status']) {
                                  case 1:
                                      $status['status'] = '<span class="glyphicon glyphicon-time" style="width: 100%; height: 100%; font-size: 40px; "></span>';
                                      $status['color'] = 'gray';
                                      break;
                                  case 2:
                                      $status['status'] = '<span class="glyphicon glyphicon-refresh" style="width: 100%; height: 100%; font-size: 40px; "></span>';
                                      $status['color'] = 'purple';
                                      break;
                                  case 3:
                                      $status['status'] = '<span class="glyphicon glyphicon-usd" style="width: 100%; height: 100%; font-size: 40px; "></span>';
                                      $status['color'] = 'green';
                                      break;
                                  case 4:
                                      $status['status'] = '<span class="glyphicon glyphicon-ok-circle" style="width: 100%; height: 100%; font-size: 40px; "></span>';
                                      $status['color'] = 'green';
                                      break;
                                  default:
                                      break;

                            }

                            ?>
                            <tr>
                                <td><?php echo $product['id']; ?></td>
                                <td align="center"><b><?php echo $product['name']; ?></b></td>
                                <td style="width: 40px;" align="center"><b><?php echo $product['price']; ?></b>
                                <td align="center"><div class="bigcontent"><?php echo $product['product_adress']; ?></div></td>
                                <td align="center"><div class="bigcontent"><?php echo $product['owner_adress']; ?></div></td>
                                <td align="center"><?php echo $product['trucknumber']; ?></td>
                                <td align="center"><div class="bigcontent"><?php echo $product['product_url']; ?></div></td>
                                <td><?php echo $product['delivery_status']; ?></td>
                                <td align="center"><?php echo $product['product_category']; ?></td>
                                <td align="center" style="color:green"><b><?php echo $product['deal_payment'];?></b></td>
                                <td align="center" style="background-color: lightgray"><b style="color: <?=  $status['color'];?>;"><?php echo $status['status']; ?></b></td>
                                <td style="width: 140px;" align="center">
                                    <?php if($product['status'] == 1 ) :?>
                                        <button type="button" class="btn btn-success"
                                                onclick="product_edit('<?php echo $product['id']; ?>')"><span class="glyphicon glyphicon-pencil"></span>
                                        </button>
                                    <?php else :?>
                                        <button type="button" class="btn btn-success" disabled><span class="glyphicon glyphicon-pencil"></span>
                                        </button>
                                    <?php endif;?>
                                    <!--<button type="button" class="btn btn-danger"
                                            onclick="product_delete('<?php //echo $product['id']; ?>')">Удалить
                                    </button>-->
                                </td>
                            </tr>
                        <?php }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>

</html>
