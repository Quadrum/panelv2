<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $data['title']; ?></title>

    <script src="../../js/jquery.min.js"></script>
    <script src="../../js/jquery.validate.min.js"></script>
    <script src="../../js/jquery-ui.min.js"></script>
    <script src="../../js/bootstrap.min.js"></script>
    <script src="../../js/jquery.dataTables.min.js"></script>
    <script src="../../js/dataTables.bootstrap.js"></script>
    <link rel="stylesheet" href="../../css/jquery-ui.css">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../../css/dataTables.bootstrap.css">
    <style type="text/css">
        #dialog-form {
            display: none;
        }
    </style>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {
            $('#d_tbl').dataTable();
            $('#d_tbl_filter').prepend('<button type="button" class="btn btn-success" onclick="user_add()">New</button>&nbsp;');
        });

        function user_add(){
            dialog = $( "#dialog-form" ).dialog({
                autoOpen: false,
                height: 700,
                width: 700,
                modal: true,
                closeOnEscape: false,
                dialogClass: "noclose",
                buttons : [
                {
                    text: "Сохранить",
                    click: function() {
                        user_save();
                    }
                },
                {
                    text: "Закрыть",
                    click: function() {
                        dialog.dialog("close");
                    }
                }
                ],
                close: function() {
                    $("#dialog-form").css("display","none");
                },
                open: function(){
                    $(".ui-dialog-titlebar-close").css("display","none");
                }
            });
            dialog.dialog("open");
        }
        function user_delete(userId) {
            $.ajax({
                type: 'POST',
                url: '/admin/user_delete',
                data:  {'id': userId},
                dataType: "json",
                success: function(data, textStatus){
                    //console.log(data);
                    location.reload();
                }
            });
            return false;
        }

        function user_save()
        {
            userdata = {
                id: $("#id").val(),
                login: $("#login").val(),
                balance: $("#balance").val(),
                password: $("#password").val(),
                is_admin: $("#is_admin").val()
            }
            $.ajax({
                type: 'POST',
                url: '/admin/user_save',
                data: userdata,
                dataType: "json",
                success: function (data, textStatus) {
                    if(data.status) {
                        dialog.dialog("close");
                        location.reload();
                    } else {
                        //console.log(data);
                    }
                },
            });

        }
        function user_edit(prodId) {
            dialog = $( "#dialog-form" ).dialog({
                autoOpen: false,
                height: 700,
                width: 700,
                modal: true,
                buttons : [
                {
                    text: "Сохранить",
                    click: function() {
                        user_save();
                    }
                },
                {
                    text: "Закрыть",
                    click: function() {
                        dialog.dialog("close");
                    }
                }
                ],
                close: function() {
                    $("#dialog-form").css("display","none");
                },
                open: function(){
                    $(".ui-dialog-titlebar-close").css("display","none");
                }
            });
            $.ajax({
                type: 'POST',
                url: '/admin/user_edit',
                data: {'userid': prodId},
                dataType: "json",
                success: function (data, textStatus) {
                   // console.log(data);
                    $("#id").val(data[0].id);
                    $("#login").val(data[0].login);
                    $("#balance").val(data[0].balance);
                    $("#is_admin option[value='"+ data[0].is_admin +"']").attr('selected', 'true');
                    dialog.dialog("open");
                }
            });
            return false;
        }
    </script>
    <!-- Custom CSS -->
    <link href="../../css/simple-sidebar.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="../../js/html5shiv.js"></script>
    <script src="../../js/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <!-- END DataTables -->
    <div id="dialog-form" title="Пользователь">
        <form>
            <fieldset>
                <div class="form-group">
                    <input type="hidden" name="id" id="id" value="" class="text ui-widget-content ui-corner-all">
                    <label for="login">Логин:</label><br>
                    <input type="text" name="login" id="login" value="" class="form-control"><br>
                    <label for="password">Пароль:</label><br>
                    <input type="text" name="password" id="password" value="" class="form-control"><br>
                    <label for="balance">Баланс:</label><br>
                    <input type="text" name="balance" id="balance" value="" class="form-control"><br>
                    <label for="product_category">Права:</label>
                    <select class="form-control" id="is_admin" name="is_admin">
                        <option value="0">Пользователь</option>
                        <option value="1">Администратор</option>
                    </select>
                </div>

                <!-- Allow form submission with keyboard without duplicating the dialog button -->
            </fieldset>
        </form>
    </div>
    <nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">Panel</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">

          <li><span class="navbar-text">Баланс : <span style="color:green"><?=$_SESSION['user_balance']?></span></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Действия <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><span class="navbar-text">Пользователь: <?=$_SESSION['user_name']?></span></li>
            <li><a href="/">Товары</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="/admin/users">Пользователи</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="/main/logout">Выход</a></li>
            <!--<li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>-->
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1>Управление пользователями</h1>
                <table class="table table-striped table-bordered" id="d_tbl">
                    <thead>
                        <th style="width: 40px;">id</th>
                        <th>Логин</th>
                        <th>Баланс</th>
                        <th>Права</th>
                        <th>Действие</th>
                    </thead>
                    <tbody>
                    <?php
                    if(!is_null($data['users'])) {
                        foreach ($data['users'] as $user) { ?>
                            <tr>
                                <td><?php echo $user['id']; ?></td>
                                <td align="center"><b><?php echo $user['login']; ?></b></td>
                                <td style="width: 40px;" align="center"><b><?php echo $user['balance']; ?></b>
                                <td><?php echo $user['is_admin'] == true ? "Администратор" : "Пользователь"; ?></td>
                            </td>
                                <td style="width: 140px;" align="center">
                                    <button type="button" class="btn btn-success"
                                            onclick="user_edit('<?php echo $user['id']; ?>')"><span class="glyphicon glyphicon-pencil"></span>
                                    </button>
                                    <button type="button" class="btn btn-danger"
                                            onclick="user_delete('<?php echo $user['id']; ?>')" disabled><span class="glyphicon glyphicon-trash"></span>
                                    </button>
                                </td>
                            </tr>
                        <?php }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>

</html>
