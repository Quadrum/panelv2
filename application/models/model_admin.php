<?php

class Model_Admin extends Model
{
    public function gUserBalId($id){
        return $this->fetchOne("SELECT balance FROM users WHERE id = ".$id)['balance'];
    }

    public function gAllUsers(){
        return $this->query("SELECT * FROM users");
    }

    public function gUserByLogin($login) {
        return $this->fetchOne("SELECT id FROM users WHERE login = '".$login."';")['id'];
    }

    public function gAllProducts(){
        //return $this->query("SELECT * FROM products WHERE archive = 0");
        return $this->query("SELECT product.*,user.login FROM products AS product INNER JOIN users AS user WHERE product.archive = 0 AND product.user_id = user.id");
    }

    public function getUser($id){
        return $this->query("SELECT id,login,balance,is_admin FROM users WHERE id = $id",MYSQLI_ASSOC);
    }

    public function setUserBalance($uid,$balance){
        return $this->query("UPDATE users SET balance =".$balance." WHERE id = $uid",MYSQLI_ASSOC);
    }

    public function getUserLogin($id){
        return $this->query("SELECT login FROM users WHERE id = $id",MYSQLI_ASSOC);
    }
    /**
     * @param $post_data
     */
    public function saveUser($post_data) {
        if($post_data['id'] == "" && !$this->gUserByLogin($post_data['login'])) {
            if($post_data['password'] != ""){

                $this->exec("INSERT INTO users (
                    login,
                    password,
                    balance,
                    is_admin) VALUES ('".
                            $post_data['login']."','".
                            md5($post_data['password'])."','".
                            $post_data['balance']."',".
                            $post_data['is_admin'].");", MYSQLI_NUM);
            } else {

                return false;
            }

        } else {
            if($post_data['password'] != ""){
                $this->exec("UPDATE users SET login = '".$post_data['login']."',
                password='".md5($post_data['password']).
                    "', balance='".$post_data['balance'].
                    "', is_admin='".$post_data['is_admin'].
                    "' WHERE id = ".$post_data['id'].
                    ";",MYSQLI_NUM);
            } else {
                $this->exec("UPDATE users SET login = '".$post_data['login']."',
                     balance='".$post_data['balance'].
                    "', is_admin='".$post_data['is_admin'].
                    "' WHERE id = ".$post_data['id'].
                    ";",MYSQLI_NUM);
            }

        }
        return true;
    }

    /**
     * @param $id post id
     * @return bool
     */
    public function  deleteUser($id){
        $this->exec("DELETE FROM users WHERE id = ".$id.";");
        return true;
    }
}