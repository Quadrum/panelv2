<?php

class Model_Product extends Model
{

    /**
     * @return array|bool
     */
    public function getAllProducts(){
        return $this->query("SELECT * FROM products",MYSQLI_ASSOC);
    }

    public function getUserProducts($id){
        return Database::query("SELECT * FROM products WHERE user_id = ".$id." AND archive = 0");
    }


    public  function setUserBalance($uid,$balance) {
        if($balance < 0 && $this->getUserBalance($uid) <= 0 ) {
            return false;
        } else {
            $this->exec("UPDATE users SET balance =" . $balance . " WHERE id =" . $uid, MYSQLI_ASSOC);
        }
    }

    public function getUserBalance($uid){
        return (double)$this->fetchOne("SELECT balance FROM users WHERE id =".$uid,MYSQLI_ASSOC)["balance"];
    }
    /**
     * @param $id поста
     * @return array|bool
     */
    public function getProduct($id) {
        return $this->query("SELECT id, user_id, name, price, product_adress, owner_adress, trucknumber, product_url, status, delivery_status, product_category, deal_payment FROM products WHERE id = $id",MYSQLI_ASSOC);
    }

    public function getAdmProduct($id,$fO=true) {
        if($fO) {
            return $this->fetchOne("SELECT * FROM products WHERE id = $id",MYSQLI_ASSOC);
        } else {
            return $this->query("SELECT * FROM products WHERE id = $id",MYSQLI_ASSOC);
        }
    }

    /**
     * @return array|bool
     */
    public function getProductsCount()
    {
        return $this->query("SELECT count(*) FROM products",MYSQLI_NUM)[0];
    }

    private function priceFormula($price,$product_category) {
        if($product_category == 'Apple') {
            if($price < 2000)  {
                $result['admin_payment'] = $price * 0.7 / 2;
                $result['deal_payment'] = $price * 0.7 / 2 - 100;
                $result['employer_payment'] = 100;
                return $result;
            } else {
                $result['admin_payment'] = $price * 0.7 / 2 - 100;
                $result['deal_payment'] = $price * 0.7 / 2;
                $result['employer_payment'] = 100;
                return $result;
            }
        } elseif($product_category == 'Не Apple')  {
            if($price < 2500)  {
                $result['admin_payment'] = $price * 0.6 / 2;
                $result['deal_payment'] = $price * 0.6 / 2 - 100;
                $result['employer_payment'] = 100;
                return $result;
            } else {
                $result['admin_payment'] = $price * 0.6 / 2 - 100;
                $result['deal_payment'] = $price * 0.6 / 2;
                $result['employer_payment'] = 100;
                return $result;
            }
        } else {

        }

    }

    /**
     * @param $post_data
     */
    public function saveProduct($post_data) {
        $payments = $this->priceFormula($post_data['price'],$post_data['product_category']);
        if($post_data['id'] == ""){
            if(isAdmin()) {
                $this->exec("INSERT INTO products ( 
                user_id,
                name,
                description,
                price,
                product_adress, 
                owner_adress, 
                trucknumber,
                product_url,
                product_category,
                deal_payment,
                employer_payment,
                admin_payment) VALUES ('".
                    $_SESSION['user_id']."','".
                    $post_data['name']."','".
                    $post_data['description']."','".
                    $post_data['price']."','".
                    $post_data['product_adress']."','".
                    $post_data['owner_adress']."','".
                    $post_data['trucknumber']."','".
                    $post_data['product_url']."','".
                    $post_data['product_category']."','".
                    $payments['deal_payment']."','".
                    $payments['employer_payment']."','".
                    $payments['admin_payment']."');",
                    MYSQLI_NUM);
            } else {
                $this->exec("INSERT INTO products ( 
                user_id,
                name,
                price,
                product_adress, 
                owner_adress, 
                trucknumber,
                product_url,
                product_category,
                deal_payment,
                employer_payment,
                admin_payment) VALUES ('".
                    $_SESSION['user_id']."','".
                    $post_data['name']."','".
                    $post_data['price']."','".
                    $post_data['product_adress']."','".
                    $post_data['owner_adress']."','".
                    $post_data['trucknumber']."','".
                    $post_data['product_url']."','".
                    $post_data['product_category']."','".
                    $payments['deal_payment']."','".
                    $payments['employer_payment']."','".
                    $payments['admin_payment']."');",
                    MYSQLI_NUM);

            }
        } else {
            if(isAdmin()) {
                $product = $this->getAdmProduct($post_data['id']);
                $userBalance = $this->getUserBalance($product['user_id']);
                $adminBalance = $this->getUserBalance(1);
                if($post_data['status'] == 3) {
                    $this->exec("UPDATE products SET  name = '" . $post_data['name'] . "',
                            description= '" . $post_data['description'] . "',
                            price='" . $post_data['price'] .
                        "', product_adress='" . $post_data['product_adress'] .
                        "', owner_adress='" . $post_data['owner_adress'] .
                        "', trucknumber='" . $post_data['trucknumber'] .
                        "', product_url='" . $post_data['product_url'] .
                        "', product_category='" . $post_data['product_category'] .
                        "', deal_payment='" . $payments['deal_payment'] .
                        "', employer_payment='" . $payments['employer_payment'] .
                        "', admin_payment='" . $payments['admin_payment'] .
                        "', status='" . $post_data['status'] .
                        "' WHERE id = " . $post_data['id'] .
                        ";", MYSQLI_NUM);
                    $userBalance += (double)$product['deal_payment'];
                    $this->setUserBalance($product['user_id'], $userBalance);
                } elseif($post_data['status'] == 4) {
                    $this->exec("UPDATE products SET name = '" . $post_data['name'] . "',
                                 description= '" . $post_data['description'] . "',
                                 price='" . $post_data['price'] .
                        "', product_adress='" . $post_data['product_adress'] .
                        "', owner_adress='" . $post_data['owner_adress'] .
                        "', trucknumber='" . $post_data['trucknumber'] .
                        "', product_url='" . $post_data['product_url'] .
                        "', product_category='" . $post_data['product_category'] .
                        "', deal_payment='" . $payments['deal_payment'] .
                        "', employer_payment='" . $payments['employer_payment'] .
                        "', admin_payment='" . $payments['admin_payment'] .
                        "', status='" . $post_data['status'] .
                        "' WHERE id = " . $post_data['id'] .
                        ";", MYSQLI_NUM);
                    $userBalance -= (double)$product['deal_payment'];
                    $adminBalance += (double)$product['admin_payment'];
                    $this->setUserBalance($product['user_id'],$userBalance);
                    $this->setUserBalance(1, $adminBalance);
                } else {
                    if(isAdmin()) {
                        $this->exec("UPDATE products SET name = '" . $post_data['name'] . "',
                                     description='" . $post_data['description'] .
                            "',     price='" . $post_data['price'] .
                            "', product_adress='" . $post_data['product_adress'] .
                            "', owner_adress='" . $post_data['owner_adress'] .
                            "', trucknumber='" . $post_data['trucknumber'] .
                            "', product_url='" . $post_data['product_url'] .
                            "', product_category='" . $post_data['product_category'] .
                            "', deal_payment='" . $payments['deal_payment'] .
                            "', employer_payment='" . $payments['employer_payment'] .
                            "', admin_payment='" . $payments['admin_payment'] .
                            "', status='" . $post_data['status'] .
                            "' WHERE id = " . $post_data['id'] .
                            ";", MYSQLI_NUM);
                    } else {

                        $this->exec("UPDATE products SET name = '" . $post_data['name'] . "',
                    price='" . $post_data['price'] .
                            "', product_adress='" . $post_data['product_adress'] .
                            "', owner_adress='" . $post_data['owner_adress'] .
                            "', trucknumber='" . $post_data['trucknumber'] .
                            "', product_url='" . $post_data['product_url'] .
                            "', product_category='" . $post_data['product_category'] .
                            "', deal_payment='" . $payments['deal_payment'] .
                            "', employer_payment='" . $payments['employer_payment'] .
                            "', admin_payment='" . $payments['admin_payment'] .
                            "', status='" . $post_data['status'] .
                            "' WHERE id = " . $post_data['id'] .
                            ";", MYSQLI_NUM);
                    }
                }
            } else {
                if(isAdmin()) {
                    $this->exec("UPDATE products SET name = '" . $post_data['name'] . "',
                             description='" . $post_data['description'] .
                        "',  price='" . $post_data['price'] .
                        "', product_adress='" . $post_data['product_adress'] .
                        "', owner_adress='" . $post_data['owner_adress'] .
                        "', trucknumber='" . $post_data['trucknumber'] .
                        "', product_url='" . $post_data['product_url'] .
                        "', product_category='" . $post_data['product_category'] .
                        "', deal_payment='" . $payments['deal_payment'] .
                        "', employer_payment='" . $payments['employer_payment'] .
                        "', admin_payment='" . $payments['admin_payment'] .
                        "' WHERE id = " . $post_data['id'] .
                        ";", MYSQLI_NUM);
                } else {
                    $this->exec("UPDATE products SET name = '" . $post_data['name'] . "',
                    price='" . $post_data['price'] .
                        "', product_adress='" . $post_data['product_adress'] .
                        "', owner_adress='" . $post_data['owner_adress'] .
                        "', trucknumber='" . $post_data['trucknumber'] .
                        "', product_url='" . $post_data['product_url'] .
                        "', product_category='" . $post_data['product_category'] .
                        "', deal_payment='" . $payments['deal_payment'] .
                        "', employer_payment='" . $payments['employer_payment'] .
                        "', admin_payment='" . $payments['admin_payment'] .
                        "' WHERE id = " . $post_data['id'] .
                        ";", MYSQLI_NUM);
                }
            }
        }
        return true;
    }

    /**
     * @param $id post id
     * @return bool
     */
    public function  archiveProduct($id){
        $this->exec("UPDATE products SET archive = 1 WHERE id = ".$id.";");
        return true;
    }
}