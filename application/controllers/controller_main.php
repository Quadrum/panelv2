<?php
class Controller_Main extends Controller
{
    function action_index()
    {
        $this->model = new Model_User();
        global $config;
        if(isset($_SESSION['inside']) && $_SESSION['inside'] == true){
             $_SESSION['user_balance'] = $this->model->gUserBalId($_SESSION['user_id']);
            if(isset($_SESSION['is_admin']) && $_SESSION['is_admin'] == true){
                $this->model = new Model_Admin();
                $products = $this->model->gAllProducts($_SESSION['user_id']);
                $this->view->generate('', 'admin_view.php',array('title' => 'Управление','products' => $products));
            } else {
                $this->model = new Model_Product();
                $products = $this->model->getUserProducts($_SESSION['user_id']);
               //  $products = $this->model->getUserProducts($_SESSION['user_id']);
                 $this->view->generate('', 'main_view.php',array('title' => 'Управление','products' => $products));
            }
        } else {
            $this->view->generate('', 'main_login.php', ['title' => 'Авторизация']);
        }
    }

    function action_login()
    {
        $this->model = new Model_User();
        $login = $this->model->login(['login' => $_POST['login'],'password' => $_POST['password']]);
        if(!is_null($login)) {
            if($login['is_admin'] == true) {
                $_SESSION['is_admin'] = true;
            } 
            $_SESSION['user_id'] = $login['id'];
            $_SESSION['user_name'] = $login['login'];
            $_SESSION['user_balance'] = $login['balance'];
            $_SESSION['inside'] = true;
            echo json_encode(array('status' => 'true','location' => ''));
        } else {
            echo json_encode(array('status' => 'false'));
        }
    }
    
    function action_logout()
    {
       session_destroy();
       header("Location: /");
    }
}