<?php
class Controller_Admin extends Controller
{
    function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['is_admin'])) {
            header("Location: /");
        }
    }


    function action_index()
    {

    }

    function action_users() {
        $this->model = new Model_Admin();
        $users = $this->model->gAllUsers();
        $this->model = new Model_User();
        $_SESSION['user_balance'] = $this->model->gUserBalId($_SESSION['user_id']);
        $this->view->generate('', 'admin_users.php',array('title' => 'Управление','users' => $users));
    }

    function action_user_delete(){
        $this->model = new Model_Admin();
        $this->model->deleteUser($_POST['id']);
        echo json_encode(array("status" => "true"));
    }

    function action_user_edit() {
        if(isset($_POST['userid'])){
            $userid = filter_var($_POST['userid'],FILTER_SANITIZE_NUMBER_INT);
            $this->model = new Model_Admin();
            echo json_encode($this->model->getUser($userid));
        }
    }

    function action_user_save(){
        $this->model = new Model_Admin();
        if($this->model->saveUser($_POST)){
            echo json_encode(array("status" => "true"));
        } else {
            echo json_encode(array("status" => "false"));
        }
    }
}