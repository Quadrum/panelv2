<?php

class Controller_Product extends Controller {

    function action_index() {

    }
    function action_product_archive(){
        $this->model = new Model_Product();
        $this->model->archiveProduct($_POST['id']);
        echo json_encode(array("status" => "true"));
    }

    function action_product_edit() {
        if(isset($_POST['prodid'])){
            $prodid = filter_var($_POST['prodid'],FILTER_SANITIZE_NUMBER_INT);
            $this->model = new Model_User();
            if(isAdmin()){
                $this->model = new Model_Product();
                echo json_encode($this->model->getAdmProduct($prodid,false));
            } else {
                $this->model = new Model_Product();
                echo json_encode($this->model->getProduct($prodid));
            }
        }
    }

    function action_product_save(){
        $this->model = new Model_Product();
        if($this->model->saveProduct($_POST)){
            echo json_encode(array("status" => "true"));
        } else {
            echo json_encode(array("status" => "false"));
        }
    }
}